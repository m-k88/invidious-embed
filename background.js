var nUrl = "https://invidio.us/embed/";
var patern = "https://*.com/embed/*"

/*LOAD*/
var getting = browser.storage.sync.get("ie_instance");
getting.then(setCurrentChoice, onError);

function onError(error) {}

function setCurrentChoice(result) {
	nUrl = result.ie_instance+"/embed/" || nUrl;
}

function newUrl(url) {
	var nu = url.split("?")
	if(nu.length > 0) {
	   nu = nu[0]
	}
	var c = nu.split("embed/")
	if(c.length > 0) {
	  nu = c[1]
	}
	return nUrl+nu+"?local=true"
}

function redirect(requestDetails) {
  var url = requestDetails.url
  console.log('H: '); console.log(requestDetails);
  if(url.includes(".youtube.") || url.includes("youtube-nocookie.")) {
	if(!url.includes("discordapp.com")) {
		url = newUrl(url)
		if(requestDetails.responseHeaders) {
			console.log("Headers: "); console.log(requestDetails.responseHeaders);
		}
		console.log("Redirection form: " + requestDetails.url+" to: "+url);
		return {
		    redirectUrl: url
		};
	}
  }
  return {}
}

browser.webRequest.onBeforeRequest.addListener(
  redirect,
  {urls:[patern], types:["sub_frame"]},
  ["blocking"]
);