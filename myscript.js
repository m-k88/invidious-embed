var iURL = "https://invidio.us/embed/";

/*LOAD*/
var getting = browser.storage.sync.get("ie_instance");
getting.then(setCurrentChoice, onError);

function onError(error) {}

function setCurrentChoice(result) {
	iURL = result.ie_instance+"/embed/" || iURL;
}

var ifs = document.body.getElementsByTagName("iframe");
var ed = document.body.getElementsByTagName("embed") || document.embeds;

replaceEmbed(ifs);
replaceEmbed(ed);

function replaceEmbed(val) {
	if(val != null) {
		for(let it of val) {
			if(it != null && !it.src.includes(iURL)) {
				replaceLink(it);
				/*if(document.location.origin.includes('.reddit.com') && !it.src.includes(iURL) && isYTURL(it.src)) {
					replaceRedditEmbed(it);
				}*/
			}
		}
	}
}

function replaceLink(n) {
	if(n && n.src != null) {
		if(n.src.includes("youtube.com/") && n.src.includes("/live_chat") == false || n.src.includes("youtube-nocookie.com/") && n.src.includes("/live_chat") == false) {
			console.log("found url: "+n.src);
			var nUrl = n.src.includes("youtube.com/") ? n.src.replace("youtube.com", "invidio.us") : ((n.src.includes("youtube-nocookie.com/") ? n.src.replace("youtube-nocookie.com", "invidio.us") : n.src));
			if(nUrl != n.src) {
				n.src = nUrl;
				var arg = "?";
				if(n.src.includes("?")) arg = "&";
				if(n.src.includes("autoplay=") == false) n.src = n.src+arg+"autoplay=false&local=true";
			}
		} else if(n.src.includes("twitter.com/i/cards/tfw/") && n.contentWindow != null && n.contentWindow.document.children != null && n.contentWindow.document.children[0] != null) {
			var nifr = n.contentWindow.document.children[0].getElementsByTagName("iframe")[0];
			if(nifr != null) {
				replaceLink(nifr);
			}
		}
	}
}

var timer = setInterval(function() {
	if(ifs != null && ifs.length < frames.length) {
		ifs = document.body.getElementsByTagName("iframe");
		replaceEmbed(ifs);
	} else if(ifs == null) {
		ifs = document.body.getElementsByTagName("iframe");
		replaceEmbed(ifs);
	} else {
		replaceEmbed(ifs);
	}
	if(ed != null && ed.length < document.embeds.length) {
		ed = document.body.getElementsByTagName("embed");
		replaceEmbed(ed);
	} else if(ed == null) {
		ed = document.body.getElementsByTagName("embed");
		replaceEmbed(ed);
	} else {
		replaceEmbed(ed);
	}
	/*if(document.location.origin.includes('.reddit.com')) {
		if(document.getElementsByClassName("media-element _3K6DCjWs2dQ93YYZDOHjib")[0] && document.getElementsByClassName("jlrhi6-1 bMGQBc")[0]) {
			var r = document.getElementsByClassName("media-element _3K6DCjWs2dQ93YYZDOHjib")[0];
			if(r.src && !r.src.includes(iURL) && document.getElementsByClassName("jlrhi6-1 bMGQBc")[0]) {
				var li = document.getElementsByClassName("jlrhi6-1 bMGQBc")[0];
				if(li.children[0] && li.children[0].href) {
					n.src = iURL+checkLink(li.children[0].href);
					/*if(li.children[0].href.includes('youtu.be')) {
						r.src = iURL+li.children[0].href.split('youtu.be/')[1];
					} else if(li.children[0].href.includes('youtube.com/watch?v=')) {
						r.src = iURL+li.children[0].href.split('youtube.com/watch?v=')[1];
					} else if(li.children[0].href.includes('youtube.com/watch/')) {
						r.src = iURL+li.children[0].href.split('youtube.com/watch/')[1];
					}°/
				}
			}
		}
		if(document.location.origin.includes('old.reddit.com') || !document.location.origin.includes('www.reddit.com') && document.location.origin.includes('.reddit.com')) {		
			if(document.getElementsByClassName("media-embed").length > 0) {
				replaceEmbedOnReddit(document.getElementsByClassName("media-embed"))	
			}
		}
		replaceEmbedOnReddit((document.body.getElementsByTagName("iframe") ||document.body.getElementsByTagName("embed") || document.embeds));
	}*/
}, 100);

/*function replaceEmbedOnReddit(val) {
	if(val != null) {
		for(let it of val) {
			if(it != null && !it.src.includes(iURL)) { replaceRedditEmbed(it); }
		}
	}
}

function replaceRedditEmbed(n) {
	if(n && n.src && !n.src.includes(iURL) && !isYTURL(n.src)) {
		if(!!n.parentNode.parentNode.parentNode.parentNode) {
			var li = /*document.getElementsByClassName("jlrhi6-1 fRrLNy")[i] ||°/ n.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("jlrhi6-1")[0] || n.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("b5szba-0 dusrSl")[0];
			if(li && li.children[0] && li.children[0].href && isYTURL(li.children[0])) {
				n.src = iURL+checkLink(li.children[0].href);
				/*if(li.children[0].href.includes('youtu.be')) {
					n.src = iURL+li.children[0].href.split('youtu.be/')[1];
				} else if(li.children[0].href.includes('youtube.com/watch?v=')) {
					n.src = iURL+li.children[0].href.split('youtube.com/watch?v=')[1];
				} else if(li.children[0].href.includes('youtube.com/watch/')) {
					n.src = iURL+li.children[0].href.split('youtube.com/watch/')[1];
				}°/
			} else if(li && li.href && isYTURL(li.href)) {
				n.src = iURL+checkLink(li.href);
			} else if(!!n.parentNode.parentNode.parentNode) {
				var li = n.parentNode.parentNode.parentNode.getElementsByClassName("b5szba-0 dusrSl")[0];
				if(li && li.href && isYTURL(li.href)) {
					n.src = iURL+checkLink(li.href);
				} else if(!!n.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode) {
					var li = n.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("b5szba-0 dusrSl")[0];
					if(li && li.href && isYTURL(li.href)) {
						n.src = iURL+checkLink(li.href);
					}
				}
			}
		} 
		if(n.parentNode.parentNode && !!n.parentNode.parentNode.children[0] && n.parentNode.parentNode.children[0] && n.parentNode.parentNode.children[0].children[0] && n.parentNode.parentNode.children[0].children[0].children[0] && isYTURL(n.parentNode.parentNode.children[0].children[0].children[0].href)) {
			var oru = n.parentNode.parentNode.children[0].children[0].children[0].href
			if(oru) {	
				n.src = iURL+checkLink(oru);
			}
		}
	}
}*/

function isYTURL(str) {
	return (str && str.includes("youtu.be") || str && str.includes("youtube.com"));
}

function checkLink(l) {
	if(l) {
		if(l.includes('youtu.be')) {
			return l.split('youtu.be/')[1];
		} else if(l.includes('youtube.com/watch?v=')) {
			return l.split('youtube.com/watch?v=')[1];
		} else if(l.includes('youtube.com/watch/')) {
			return l.split('youtube.com/watch/')[1];
		} else if(l.includes('u=%')) { //check fopr attribution_link
			return decodeURIComponent(l.split('u=')[1]).split('&')[0].split('v=')[1];
		}
	}
	return l;
}
