(function() {
	var iUrl = "https://invidio.us/"
	/*INIT*/
	var error = document.getElementById("error")
	var input = document.getElementById("instance")
	var btns = document.getElementById("btn_save")
	btns.addEventListener("click", function(e) {
		if(input && input.value != null) {
			var v = input.value
			if(!v.startsWith("http://") || !v.startsWith("https://")) {
				v = "http://"+v
			}
			if(!v.endsWith("/")) {
				v = v+"/"
			}
			if(v.includes("/embed")) {
				v = v.replace("/embed", "")
			}
			browser.storage.sync.set({
				   ie_instance: input.value
			});
		}
	})
	/*LOAD*/
	var getting = browser.storage.sync.get("ie_instance");
	getting.then(setCurrentChoice, onError);

	function onError(error) { /*input.value = iUrl;*/ }

	function setCurrentChoice(result) {
	    input.value = result.ie_instance || iUrl;
	}
})()
